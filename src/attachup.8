.\" attachup - Backup on Attach
.TH "attachup" "8" ""  "Klaatu" ""
.SH "NAME"
attachup \- Backup portable drives when attached
.SH "DESCRIPTION"
.PP 
The \fBattachup\fP philosophy is that you carry your data on a
portable drive, which gets backed-up to your computers. This deviates
from the tradition of backing-up computers to portable drives, but it
has important advantages: the latest version of your data is
centralised with automated redundant backups spread across on your
devices. If you lose or corrupt a file, you can restore it from
whatever computer you are using, and conversely if the computer
suffers a catastrophic crash, your important data is safe on your
portable drive.
.PP
.SH Setup
The first thing you need is a good-sized portable drive to house your
personal data. Thumbdrives, or pendrives, have gotten quite large and
are easy to attach to your key chain, and large-capacity drives are
only the size of a passport or pocketbook.
.PP
If it isn't formatted already, format your drive and give it a unique
file system label. How you assign a label depends on the file system
you use. For example:
.IP
mkfs.ext4 -L exampledrive /dev/sdx1
.PP
Refer to \fBmkfs\fP documentation for more information.
.PP
You can also change a label on an existing drive with \fBmlabel\fP or
the appropriate utils package, such as \fBjfs-utils\fP for JFS or
\fBe2label\fP for EXT. A thorough overview of how to do this is
located on https://help.ubuntu.com/community/RenameUSBDrive
.PP
The file system label can be anything you want, and it will be the unique
identifier for this drive. It is helpful if it is unique among the
hard drives that you own or use on a regular basis. It also simplifies
your life if it is easy for a POSIX system to handle; avoid putting a
space in the name, or special characters. Go with something simple,
especially if you are not experienced with character escaping.
.PP
Move your important data from your $HOME directory onto your
drive. When working, work off of this drive, not off the drive in your
computer.
.PP
.SH REQUIREMENTS
.TP
\fBAttachup\fP requires Python3, udev, pyudev, rdiff-backup, librsync.
.PP

.SH INSTALLATION
.PP
To install \fBattachup\fP, use the included SlackBuild file, or just
copy the attachup udev rule to /etc/udev/rules.d. You can rename the
attachup udev rule file, or merge it with an existing one, if you
already have a custom rule file.
.PP
Regardless of how you have installed attachup, you must customise
the udev script for improved drive identification and install backup
scripts that match the drive you want to backup.
.PP
Customising \fBattachup\fP can be done manually or using the
\fBattachup\fP command. 
.PP
.SH REQUIRED COMMAND OPTIONS
.PP
The \fBattachup\fP command adds and removes a udev entry and a backup
script for a drive that you want to backup.
.PP
There are 5 options that you probably need to pass to the \fBattachup\fP command.
.PP
.B -i, --uuid
.IP
The UUID of your drive. If you do not provide this, \fBattachup\fP
dumps the UUIDs it detects to a terminal so you can find the one you
want to use.
.TP
Sometimes \fBattachup\fP fails to detect the UUID of a drive. You can get
this information yourself using the \fBlsblk\fP and \fBudevadm info\fP
commands. First, get the device identifier for your drive with:
.IP
lsblk
.PP
Then get the UUID for the partition you want to backup. Assuming your
device is /dev/sdx1, for example: 
.IP
udevadm info /dev/sdx1 | grep ID_FS_UUID= | cut -f2 -d"="
.TP
.B -l, --label
.IP
The label of your drive. If you do not provide this, \fBattachup\fP
dumps the UUIDs it detects to a terminal so you can find the one you
want to use.
.TP
.B -p, --path
.IP
Path to backup location. By default, /var/attachups, but if you are the
only user on your computer, you're free to put in your home
directory for maximum visibility.
.TP
.B -u, --user
.IP
The user who owns the drive. This should be a valid username for the
system, and it defines who has access to the backup folder.
.TP
.B -g, --group
.IP
The group that owns the drive. This should be a valid group for the
system, and it defines who has access to the backup folder.
.PP

.SH ACTIONS
.PP
You also need to tell \fBattachup\fP whether you want to add or delete
a rule.
.TP
.B -a, --add
.IP
Add the drive to udev, and install its the backup script.
.TP
.B -d, --delete
.IP
Remove the drive\'s attachup udev rule, and uninstall its backup script.
.PP
.SH CONVENIENCE OPTIONS
.PP
If you are a sys admin or you maintain a very unique system of your
own design, there are some uncommon options, too.
.TP
.B -r, --rules
.IP
Path to your udev directory. On most systems, this is the default: /etc/udev/rules.d
.TP
.B -c, --conf
.IP
Path to a nonstandard attachup.conf file. By default,
/etc/attachup.conf. You probably do not need to change this.
.TP
.B -e, --exec
.IP
Path where you want the backup scripts installed. By default:
/usr/local/bin. This gets explicitly set in the backup script, so the
exec path does not need to be in your $PATH to work.
.TP
.B -t, --permanent
.IP
Write the options of your current command to attachup.conf. The next
time you add a drive for user foo, you only need to define the
username, label, and uuid. All other options are read from
attachup.conf for that user.
.PP

.SH Configuring the udev script manually
.PP 
\fBAttachup\fP udev rules identifies your drive by its file system
label, which you created or modified during the setup phase. It
confirms the identity of a drive bearing that label with the partition
UUID.
.PP
To find the UUID of your mountable partition, plug your drive in, find
it with \fBlsblk\fP (assume it is /dev/sdx, with partition /dev/sdx1,
for this example), and then look at its udev info:
.IP
udevadm info /dev/sdx1 | grep ID_FS_UUID= | cut -f2 -d"="
.PP
Replace the string UUID_OF_YOUR_DRIVE in the udev rule with your
drive's UUID. If you have more than one drive or more than one
partition you wish to backup, repeat this process, copying and pasting
the backup line with a new UUID.
.PP
.SH Configuring the backup script
You must modify one of the example backup scripts, replacing
YOUR-USERNAME with your username and EXAMPLE with the file system
label of your drive.
.PP
Save the backup script using the file system label of your drive
followed by -backup.sh, to the /usr/local/bin directory, and grant it
executable permissions. For example:
.IP
chmod +x /usr/local/bin/silver128gb-backup.sh
.PP
Now you are ready to use \fBattachup\fP.
.PP

.SH Using attachup
.PP
\fBAttachup\fP is fully automated. When you plug in your drive,
\fBattachup\fP detects the drive, identified primarily by its UUID but
given a name based on its file system label, mounts it, and then backs
it up using \fBrdiff-backup\fP to your designated backup location.
.PP
The first time you attach your drive, the backup process may take
several minutes or even hours, depending on how much data you\'ve
got.
.PP
Subsequent times vary. Even with small changes to your drive, there
may be a noticeable delay when you attach your drive. It depends on
the speed of your drive, whether you are using USB 2 or USB 3, and so
on.
.PP

.SH Restoring from backup
.PP
\fBAttachup\fP uses rdiff-backup for the actual backup tasks, so if
you need to restore a file, see the documentation for
rdiff-backup. Here's a quick reference, in case you need the info
RIGHT NOW:
.PP 
Assume your data is at $HOME/exampledrive.rdiff
.PP
To restore the latest good version of a file:
.IP
rdiff-backup --restore-as-of now $HOME/exampledrive.rdiff/foo.txt
~/foo.txt
.PP
or just:
.IP
rdiff-backup -r now $HOME/exampledrive.rdiff/foo.txt ~/foo.txt
.PP
To restore the a file as it appeared two days ago:
.IP
rdiff-backup -r 2D $HOME/exampledrive.rdiff/foo.txt ~/foo-2D.txt
.PP
See rdiff-backup docs for all of its many options.


.SH "SEE ALSO"
.nf
.I attachup.conf (5)
.I rdiff-backup (1)
.I udev (7)
.I librsync (3)
.URL http://rdiff-backup.nongnu.org
.fi

.PP
.SH "AUTHORS"
.nf
Klaatu (klaatu@member.fsf.org)
.fi

.PP
.SH "BUGS"
Report via email or on gitlab.com, or fix them and
request a merge.
.fi
